<?php 

/**
 *Plugin Name: Restrict Content Based On Purchased WC Product
 *Description: Only display page content if the current user has purchased the specified product or products.
 *Author: Greg Lorenzen
 *Version: 1.0.1
**/

function restrict_wc_shortcode($atts = [], $content = null, $tag = '')
{
    $atts = array_change_key_case((array) $atts, CASE_LOWER);

    $o = '';

    $o .= '<div class="restrict-content-box">';

    $current_user = wp_get_current_user();

    if (current_user_can('administrator') || wc_customer_bought_product($current_user->email, $current_user->ID, $atts['pid'])){
        if (!is_null($content)) {
            $o .= apply_filters('the_content', $content);
        }
    } else {
        $o .= '<div>You are not authorized to view this content.</div>';
    }

    $o .= '</div';

    return $o;
}

add_shortcode('wcr', 'restrict_wc_shortcode');